// Plugin for Scalastyle
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "1.0.0")

// Plugin for Scoverage
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.6.1")

// this v1.5.1 is preinstalled in current IDEA releases, used for code formatting
addSbtPlugin("com.geirsson" % "sbt-scalafmt" % "1.5.1")

// Plugins to make fat Jar
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.9")

// Native packager
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.7.6")
