FROM openjdk:8-alpine

WORKDIR /app

COPY akka-sample-assembly-*.jar akka-sample-assembly-0.1.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","akka-sample-assembly-0.1.jar"]
