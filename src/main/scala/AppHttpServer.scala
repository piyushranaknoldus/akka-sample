import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer

import scala.util.{Failure, Properties, Success}

object AppHttpServer extends App with AkkaSampleRoutes {

  implicit val system: ActorSystem = ActorSystem("akka-sample")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  import system.dispatcher

  val port = Properties.envOrElse("PORT", "8080").toInt
  //bind route to server
  val binding = Http().newServerAt("0.0.0.0", port).bind(sampleRoute)

  binding.onComplete {
    case Success(binding) ⇒
      val localAddress = binding.localAddress
      print(
        s"Server is listening on ${localAddress.getHostName}:${localAddress.getPort}")
    case Failure(e) ⇒
      print(s"Binding failed with ${e.getMessage}")
      system.terminate()
  }
}
