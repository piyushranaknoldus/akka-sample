import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

import scala.util.Random

trait AkkaSampleRoutes {

  /**
    * Sample request to check HTTP connection
    */
  def sampleRoute: Route =
    path("health") {
      get {
        val result = "ok"
        complete(result)
      }
    } ~
      get {
        val htmlPageContent = s"""<!DOCTYPE html>
                                |<html>
                                |<body>
                                |
                                |<h2>Random No Generator</h2>
                                |<p>Here is a random no: ${Random
                                   .nextInt()
                                   .toString}</p>
                                |<p> by Akka-Sample HTTP!</p>
                                |<p> Knoldus Inc!</p>
                                |<p> By Piyush Rana</p>
                                |<p> Check out my <a href="https://www.linkedin.com/in/piyush-rana-70027888/">LinkedIn Profile</a> to know more about me, or my other <a href="https://blog.knoldus.com/author/bigtechnologies/">Blogs</a></p>
                                |<img src="https://www.knoldus.com/assets/img/ui/header/knoldus-logo-new.png" alt="www.knoldus.com" width="204" height="122">
                                |
                                |</body>
                                |</html>
                                |""".stripMargin
        complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, htmlPageContent))
      }
}
