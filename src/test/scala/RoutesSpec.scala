
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{Matchers, WordSpec}

class RoutesSpec extends WordSpec with Matchers with AkkaSampleRoutes with ScalatestRouteTest {

  val route: Route = sampleRoute

  "akka sample service" should {
    "return ok route when health is hit" in {
      Get("/health") ~> route ~> check {
        status shouldEqual StatusCodes.OK
      }
    }

    "return ok route when random no generator route is hit" in {
      Get("/") ~> route ~> check {
        status shouldEqual StatusCodes.OK
      }
    }
  }
}